function printList(array) {
    if(array.length === 0) return;

    function getListFromArray(array) {

        let li = array.map(el=> {
            if (typeof el === 'object' && el !== null) {
                return getListFromArray(el);
            }
            else {
                return `<li>${el}</li>`;
            }
        });
        return `<ul>${li.join('')}</ul>`;
    }

    function print(string){
        document.body.innerHTML += string;
    }
    let list = getListFromArray(array);
    print(list);

}
function createTimer(seconds = 10) {
    let intervalId;
    let p = document.createElement('p');
    document.body.prepend(p);
    runTimer();


    function outputValue(p, seconds){
        p.textContent = seconds;
    }
    function clearBody(){
        document.body.textContent = '';
    }

    function runTimer() {
        intervalId = setInterval(() => {
            seconds--;
            outputValue(p, seconds);
            if (seconds < 1) {
                clearInterval(intervalId);
                clearBody();
            }
        }, 1000);

    }

}
    let array = ['hello', 'world', 'Kiev', 'Kharkiv',['1', 2,['fssdf', 'sad', 'dscdd'], 56], 'Odessa', 'Lviv'];
    printList(array);
    createTimer(10);

